import {StatusBar} from 'expo-status-bar';
import React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

const Stack = createStackNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={Home}/>
                <Stack.Screen name="ButtonDetails" component={ButtonDetails}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}

function Home({navigation}) {
    return (
        <View style={styles.container}>
            <Button
                style={styles.button}
                title="Press Me"
                onPress={() => {
                    navigation.navigate('ButtonDetails');
                }}
            >Press me</Button>
            <Text>Open up App.js to start working on your app!</Text>
            <StatusBar style="auto"/>
        </View>
    );
}

function ButtonDetails() {
    return (
        <View>
            <Text>Boo</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    button: {
        backgroundColor: '#555'
    }
});
