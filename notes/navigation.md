# Navigating multiple activities

## Individual activities
Each activity can be put in a function, that returns the React elements that make up the activity. For example:

```js
function CustomerDetails() {
    return (
        <View>
            <Text>Syntaxis</Text>
            <Text>Glasbak</Text>
            <Text>Enschede</Text>
        </View>
    );
}
```

If the above code would be put in the App() function, you'll have this displayed when launching the app.

## NavigationContainer
```js
export default function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Home">
                <Stack.Screen name="Home" component={Home}/>
                <Stack.Screen name="ButtonDetails" component={ButtonDetails}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}
```

The code above defines all activities (where Home and ButtonDetails are functions that define activities)
The initial route defines what activity we start on.
If a certain activity needs to have navigation options, for example a button click opening a new activity:

```js
function Home({navigation}) {
    return (
        <View>
            <Button
                title="Press Me"
                onPress={() => {
                    navigation.navigate('ButtonDetails');
                }}
            >Press Me</Button>
        </View>
    )
}
```

This will navigate to the activity defined as ButtonDetails.